<%--
  Created by IntelliJ IDEA.
  User: axbat
  Date: 03/02/2019
  Time: 07:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Films</title>
</head>
<body>
<%@ include file="include/navbar.jsp" %>

<h1>Les films</h1>

<div class="well">
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th>ID</th>
            <th>Titre</th>
            <th>Durée</th>
            <th>Date de sortie</th>
            <th>Budget</th>
            <th>Recette</th>
            <th>Réalisateur</th>
            <th>Catégorie</th>
            <th>Informations</th>
            <th>Supprimer</th>
        </tr>
        </thead>
        <c:forEach items="${mesFilms}" var="unFilm">
        <tr>
            <td>${unFilm.getNoFilm()}</td>
            <td>${unFilm.getTitre()}</td>
            <td>${unFilm.getDuree()} minutes</td>
            <td>${unFilm.getDateSortie()}</td>
            <td>${unFilm.getBudget()} $</td>
            <td>${unFilm.getMontantRecette()} $</td>
            <td> ${unFilm.getRealisateurByNoRea().getPrenRea()} ${unFilm.getRealisateurByNoRea().getNomRea()}</td>
            <td>${unFilm.getCategorieByCodeCat().getLibelleCat()}</td>
            <td style="text-align:center;">
                <a class="glyphicon glyphicon-search" data-toggle="tooltip" data-placement="top"
                   title="Informations" href="#"
                        onclick="window.location ='/informationFilm/${unFilm.getNoFilm()}';">
                </a>
            </td>
            <td style="text-align:center;">
                <a class="glyphicon glyphicon-remove-sign" data-toggle="tooltip" data-placement="top"
                   title="Supprimer" href="#"
                   onclick="javascript:if (confirm('Suppression confirmée ?'))
                           window.location ='/supprimerFilm/${unFilm.getNoFilm()}';">
                </a>
            </td>


        </tr>
        </c:forEach>
</body>
</html>
