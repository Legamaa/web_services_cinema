<%--
  Created by IntelliJ IDEA.
  User: axbat
  Date: 04/02/2019
  Time: 08:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Fiche d'information</title>
</head>
<body>
<%@ include file="include/navbar.jsp" %>
<h1>Fiche d'information pour ${monFilm.getTitre()}</h1>
<table class="table table-bordered table-striped">
    <tr>
        <td>Numéro du film :</td>
        <td>${monFilm.getNoFilm()}</td>

    </tr>
    <tr>
        <td>Date de sortie :</td>
        <td>${monFilm.getDateSortie()}</td>
    </tr>
    <tr>
        <td>Durée :</td>
        <td>${monFilm.getDuree()} minutes</td>
    </tr>
    <tr>
        <td>Budget :</td>
        <td>${monFilm.getBudget()} $</td>
    </tr>
    <tr>
        <td>Recette :</td>
        <td>${monFilm.getMontantRecette()} $</td>
    </tr>
    <tr>
        <td>Réalisteur</td>
        <td>${monFilm.getRealisateurByNoRea().getNomRea()}  ${monFilm.getRealisateurByNoRea().getPrenRea()}</td>
    </tr>
    <tr>
        <td>Catégorie :</td>
        <td>${monFilm.getCategorieByCodeCat().getLibelleCat()}</td>
    </tr>
    <tr>
        <td>Personnages :</td>
        <td>
            <ul>
                <c:forEach items="${mesPersos}" var="unPerso">
                    <li>${unPerso.getNomPers()} est joué par ${unPerso.getActeurByNoAct().getPrenAct()} ${unPerso.getActeurByNoAct().getNomAct()} </li>
                </c:forEach>
            </ul>
        </td>
    </tr>
    </tr>
</table>

</body>
</html>
