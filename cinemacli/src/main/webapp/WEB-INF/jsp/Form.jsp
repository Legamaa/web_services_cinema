<%--
  Created by IntelliJ IDEA.
  User: axbat
  Date: 04/02/2019
  Time: 18:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Formulaire ajout film</title>
</head>
<body>
<%@ include file="include/navbar.jsp" %>

<form action = "/submitForm"   method = "POST">
    Nom du film <input path="titre"  id="titre" type = "text" name = "titre" required class="form-control">
    <br />
    Durée (minutes): <input id="duree" type = "number" name = "duree" required min="0" class="form-control"/>
    <br />
    Date de sortie: <input  id="date" type = "date" name = "date" required class="form-control"/>
    <br />
    Budget ($): <input  id="budget" type = "number" name = "budget" required min="0" class="form-control"/>
    <br />
    Recette ($): <input id="recette" type = "number" name = "recette" min="0" required class="form-control"/>
    <br />
    Realisateurs: <select   id="realisateur" name = "realisateur" class="form-control">
    <c:forEach items="${mesReals}" var = "unReal">
        <option  value = ${unReal.getNoRea()} >${unReal.getPrenRea()} ${unReal.getNomRea()}</option>
    </c:forEach>
    </select>
    <BR>
    Categorie: <select id="categorie" name = "categorie" class="form-control">
    <c:forEach items="${mesCats}" var = "uneCat">
        <option  value = ${uneCat.getCodeCat()} >${uneCat.getLibelleCat()}</option>
    </c:forEach>
    </select>
    <BR>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>


</body>
</html>
