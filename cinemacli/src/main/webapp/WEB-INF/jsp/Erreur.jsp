<%@ page language="java" contentType="text/html; charset=ISO-8859-1" %>

<!DOCTYPE html>
<html>

<%@ include file="include/navbar.jsp" %>
<div class="col-md-12 well well-md">
    <center><h1>Gestion des erreurs </h1></center>
</div>
<if test="${MesErreurs != null }">
    <div class="alert-danger" role="alert">
        <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
        <out value="${MesErreurs}" />
        <h1>${MesErreurs}</h1>
    </div>
</if>

<div class="form-group">
    <div class="col-md-6 col-md-offset-3">
        <button type="button" class="btn btn-default btn-primary"  onclick="relocate_home()">
            <span class="glyphicon glyphicon-log-in"></span>
            Valider
        </button>
    </div>
</div>




</body>

<script>
    function relocate_home()
    {
        location.href = "/";
    }
</script>
</html>




