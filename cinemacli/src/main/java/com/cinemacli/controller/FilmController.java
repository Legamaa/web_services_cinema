package com.cinemacli.controller;

import com.cinemacli.consommation.Appel;
import com.cinemacli.domains.CategorieEntity;
import com.cinemacli.domains.FilmEntity;
import com.cinemacli.domains.PersonnageEntity;
import com.cinemacli.domains.RealisateurEntity;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.lang.reflect.Type;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Controller
public class FilmController {
/*************************************************/
/**************Tous les films ******************/
    /*************************************************/

    @RequestMapping(method = RequestMethod.GET, value = "/getTousLesFilms")
    public ModelAndView appelTousLesseFilms(ModelMap mode) throws Exception {

        String destinationPage = "";
        String ressource = "/getFilms";
        String reponse;
        try {
            Appel unAppel = new Appel();
            System.out.println(1);
            reponse = unAppel.getJson(ressource);
            System.out.println(reponse);
            System.out.println(2);
            Gson gson = new Gson();
            System.out.println(3);
            Type listType = new TypeToken<ArrayList<Object[]>>(){}.getType();
            System.out.println(4);

            Type collectionType = new TypeToken<Collection<FilmEntity>>(){}.getType();
            System.out.println(4.5);
            Collection<FilmEntity> enums = gson.fromJson(reponse, collectionType);

            //List<Object[]> mesFilmssjson = new Gson().fromJson(reponse, listType);
            System.out.println(5);
            mode.addAttribute("mesFilms", enums);
            System.out.println(6);
            destinationPage = "Films";
            System.out.println(7);
        } catch (Exception e) {
            mode.addAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";
        }
        return new ModelAndView(destinationPage,mode);
    }

    /*************************************************/
    /**************Delete un film ******************/
    /*************************************************/

    @RequestMapping(method = RequestMethod.GET, value = "/supprimerFilm/{id}")
    public ModelAndView deleteUnFilm(ModelMap mode, @PathVariable(value = "id") int id) throws Exception {

        String destinationPage = "";
        String ressource1 = "/deleteFilm/" + id;
        String ressource2 ="/getFilms";
        String reponse1;
        String reponse2;
        try {
            Appel unAppel = new Appel();
            System.out.println(1);
            reponse1 = unAppel.postJson(ressource1, null);
            System.out.println(2);
            destinationPage = "home";
            unAppel.close();

            /*Appel unAppel2 = new Appel();
            reponse2 = unAppel2.getJson(ressource2);
            Gson gson = new Gson();
            System.out.println(3);
            Type listType = new TypeToken<ArrayList<Object[]>>(){}.getType();
            System.out.println(4);

            Type collectionType = new TypeToken<Collection<FilmEntity>>(){}.getType();
            System.out.println(4.5);
            Collection<FilmEntity> enums = gson.fromJson(reponse2, collectionType);

            //List<Object[]> mesFilmssjson = new Gson().fromJson(reponse, listType);
            System.out.println(5);
            mode.addAttribute("mesFilms", enums);
            System.out.println(6);
            destinationPage = "Films";
            System.out.println(7);*/
        } catch (Exception e) {
            mode.addAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";
        }
        return appelTousLesseFilms(mode);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/informationFilm/{id}")
    public ModelAndView infoFilm(ModelMap mode, @PathVariable(value = "id") int id) throws Exception {

        String destinationPage = "";
        String ressource1 = "/getFilm/" + id;
        String ressource2 = "/getPersonnagesOfFilm/" + id;
        String reponse1;
        String reponse2;
        try {
            Appel unAppel = new Appel();
            Gson gson = new Gson();
            reponse1 = unAppel.getJson(ressource1);
            FilmEntity lefilm = gson.fromJson(reponse1, FilmEntity.class);
            reponse2 = unAppel.getJson(ressource2);
            Type collectionType = new TypeToken<Collection<PersonnageEntity>>(){}.getType();
            Collection<PersonnageEntity> enums = gson.fromJson(reponse2, collectionType);

            mode.addAttribute("mesPersos", enums);
            mode.addAttribute("monFilm", lefilm);
            destinationPage = "Info";
        } catch (Exception e) {
            mode.addAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";
        }
        return new ModelAndView(destinationPage,mode);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/formAjoutFilm")
    public ModelAndView formAjoutFilm( ModelMap mode)throws Exception {

        String destinationPage = "";
        String ressource1 = "/getRealisateurs" ;
        String ressource2 = "/getCategories" ;
        String reponse1;
        String reponse2;
        try {
            Appel unAppel = new Appel();
            Gson gson = new Gson();

            reponse1 = unAppel.getJson(ressource1);
            Type collectionType = new TypeToken<Collection<RealisateurEntity>>(){}.getType();
            Collection<RealisateurEntity> realisateurs = gson.fromJson(reponse1, collectionType);

            reponse2 = unAppel.getJson(ressource2);
            Type collectionType2 = new TypeToken<Collection<CategorieEntity>>(){}.getType();
            Collection<CategorieEntity> categories = gson.fromJson(reponse2, collectionType2);

            mode.addAttribute("mesCats", categories);
            mode.addAttribute("mesReals", realisateurs);
            destinationPage = "Form";
        } catch (Exception e) {
            mode.addAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";
        }
        return new ModelAndView(destinationPage,mode);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/submitForm")
    public ModelAndView ajoutFilm(ModelMap mode, @RequestParam("titre") String titre, @RequestParam("realisateur") int Noreal,
                                  @RequestParam("categorie") String cat, @RequestParam("duree") int duree,
                                  @RequestParam("budget") int budget, @RequestParam("date") Date date,
                                  @RequestParam("recette") int recette)throws Exception {

        String destinationPage = "";
        String ressource1 = "/getRealisateur/" + Noreal ;
        String ressource2 = "/getCat/"+cat ;
        String ressource3 = "/getFilms" ;
        String ressource4 = "/addFilm" ;
        String reponse1;
        String reponse2;
        String reponse3;
        FilmEntity leFilm = new FilmEntity();

        try {
            Appel unAppel = new Appel();


            Gson gson = new Gson();
            reponse1 = unAppel.getJson(ressource1);
            RealisateurEntity leRea = gson.fromJson(reponse1, RealisateurEntity.class);

            gson = new Gson();
            reponse2 = unAppel.getJson(ressource2);
            CategorieEntity laCat = gson.fromJson(reponse2, CategorieEntity.class);

            leFilm.setBudget(budget);
            leFilm.setCategorieByCodeCat(laCat);
            leFilm.setCodeCat(cat);
            leFilm.setDateSortie(date.toString());
            leFilm.setDuree(duree);
            leFilm.setMontantRecette(recette);
            leFilm.setNoRea(Noreal);
            leFilm.setRealisateurByNoRea(leRea);
            leFilm.setTitre(titre);

            reponse3 = unAppel.getJson(ressource3);
            Type listType = new TypeToken<ArrayList<Object[]>>(){}.getType();
            Type collectionType = new TypeToken<Collection<FilmEntity>>(){}.getType();
            Collection<FilmEntity> enums = gson.fromJson(reponse3, collectionType);
            int max = 0;
            for(FilmEntity f: enums){
                if (f.getNoFilm()>max){
                    max = f.getNoFilm();
                }
            }
            leFilm.setNoFilm(max +1);

            unAppel.postJson(ressource4, leFilm);
            unAppel.close();

            destinationPage = "home";




        } catch (Exception e) {
            mode.addAttribute("MesErreurs", e.getMessage());
            destinationPage = "Erreur";
        }
        return appelTousLesseFilms( mode);
    }

}