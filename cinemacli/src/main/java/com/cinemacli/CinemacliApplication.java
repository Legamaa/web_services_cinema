package com.cinemacli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CinemacliApplication {

    public static void main(String[] args) {
        SpringApplication.run(CinemacliApplication.class, args);
    }

}

