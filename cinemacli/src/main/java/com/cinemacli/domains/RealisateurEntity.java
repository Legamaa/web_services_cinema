package com.cinemacli.domains;

import java.util.Objects;

public class RealisateurEntity {
    private int noRea;
    private String nomRea;
    private String prenRea;


    public int getNoRea() {
        return noRea;
    }

    public void setNoRea(int noRea) {
        this.noRea = noRea;
    }

    public String getNomRea() {
        return nomRea;
    }

    public void setNomRea(String nomRea) {
        this.nomRea = nomRea;
    }

    public String getPrenRea() {
        return prenRea;
    }

    public void setPrenRea(String prenRea) {
        this.prenRea = prenRea;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RealisateurEntity that = (RealisateurEntity) o;
        return noRea == that.noRea &&
                Objects.equals(nomRea, that.nomRea) &&
                Objects.equals(prenRea, that.prenRea);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noRea, nomRea, prenRea);
    }

}
