package com.cinemacli.domains;


import java.sql.Date;
import java.util.Objects;


public class ActeurEntity {
    private int noAct;
    private String nomAct;
    private String prenAct;
    private String dateNaiss;
    private String dateDeces;


    public int getNoAct() {
        return noAct;
    }

    public void setNoAct(int noAct) {
        this.noAct = noAct;
    }


    public String getNomAct() {
        return nomAct;
    }

    public void setNomAct(String nomAct) {
        this.nomAct = nomAct;
    }


    public String getPrenAct() {
        return prenAct;
    }

    public void setPrenAct(String prenAct) {
        this.prenAct = prenAct;
    }


    public String getDateNaiss() {
        return dateNaiss;
    }

    public void setDateNaiss(String dateNaiss) {
        this.dateNaiss = dateNaiss;
    }


    public String getDateDeces() {
        return dateDeces;
    }

    public void setDateDeces(String dateDeces) {
        this.dateDeces = dateDeces;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ActeurEntity that = (ActeurEntity) o;
        return noAct == that.noAct &&
                Objects.equals(nomAct, that.nomAct) &&
                Objects.equals(prenAct, that.prenAct) &&
                Objects.equals(dateNaiss, that.dateNaiss) &&
                Objects.equals(dateDeces, that.dateDeces);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noAct, nomAct, prenAct, dateNaiss, dateDeces);
    }

}
