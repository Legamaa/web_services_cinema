package com.cinemacli.domains;


import java.sql.Date;
import java.util.Objects;


public class FilmEntity {
    private int noFilm;
    private String titre;
    private int duree;
    private String dateSortie;
    private int budget;
    private int montantRecette;
    private int noRea;
    private String codeCat;
    private RealisateurEntity realisateurByNoRea;
    private CategorieEntity categorieByCodeCat;

    public int getNoFilm() {
        return noFilm;
    }

    public void setNoFilm(int noFilm) {
        this.noFilm = noFilm;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    public String getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(String dateSortie) {
        this.dateSortie = dateSortie;
    }

    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    public int getMontantRecette() {
        return montantRecette;
    }

    public void setMontantRecette(int montantRecette) {
        this.montantRecette = montantRecette;
    }

    public int getNoRea() {
        return noRea;
    }

    public void setNoRea(int noRea) {
        this.noRea = noRea;
    }

    public String getCodeCat() {
        return codeCat;
    }

    public void setCodeCat(String codeCat) {
        this.codeCat = codeCat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilmEntity that = (FilmEntity) o;
        return noFilm == that.noFilm &&
                duree == that.duree &&
                budget == that.budget &&
                montantRecette == that.montantRecette &&
                noRea == that.noRea &&
                Objects.equals(titre, that.titre) &&
                Objects.equals(dateSortie, that.dateSortie) &&
                Objects.equals(codeCat, that.codeCat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noFilm, titre, duree, dateSortie, budget, montantRecette, noRea, codeCat);
    }

    public RealisateurEntity getRealisateurByNoRea() {
        return realisateurByNoRea;
    }

    public void setRealisateurByNoRea(RealisateurEntity realisateurByNoRea) {
        this.realisateurByNoRea = realisateurByNoRea;
    }

    public CategorieEntity getCategorieByCodeCat() {
        return categorieByCodeCat;
    }

    public void setCategorieByCodeCat(CategorieEntity categorieByCodeCat) {
        this.categorieByCodeCat = categorieByCodeCat;
    }

}
