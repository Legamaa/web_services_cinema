package com.cinemacli.domains;

import java.util.Objects;


public class CategorieEntity {
    private String codeCat;
    private String libelleCat;

    public String getCodeCat() {
        return codeCat;
    }

    public void setCodeCat(String codeCat) {
        this.codeCat = codeCat;
    }

    public String getLibelleCat() {
        return libelleCat;
    }

    public void setLibelleCat(String libelleCat) {
        this.libelleCat = libelleCat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategorieEntity that = (CategorieEntity) o;
        return Objects.equals(codeCat, that.codeCat) &&
                Objects.equals(libelleCat, that.libelleCat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(codeCat, libelleCat);
    }

}
