package com.cinemacli.domains;


import java.util.Objects;


public class PersonnageEntity {
    private int noFilm;
    private int noAct;
    private String nomPers;
    private FilmEntity filmByNoFilm;
    private ActeurEntity acteurByNoAct;

    public int getNoFilm() {
        return noFilm;
    }

    public void setNoFilm(int noFilm) {
        this.noFilm = noFilm;
    }

    public int getNoAct() {
        return noAct;
    }

    public void setNoAct(int noAct) {
        this.noAct = noAct;
    }

    public String getNomPers() {
        return nomPers;
    }

    public void setNomPers(String nomPers) {
        this.nomPers = nomPers;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PersonnageEntity that = (PersonnageEntity) o;
        return noFilm == that.noFilm &&
                noAct == that.noAct &&
                Objects.equals(nomPers, that.nomPers);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noFilm, noAct, nomPers);
    }

    public FilmEntity getFilmByNoFilm() {
        return filmByNoFilm;
    }

    public void setFilmByNoFilm(FilmEntity filmByNoFilm) {
        this.filmByNoFilm = filmByNoFilm;
    }

    public ActeurEntity getActeurByNoAct() {
        return acteurByNoAct;
    }

    public void setActeurByNoAct(ActeurEntity acteurByNoAct) {
        this.acteurByNoAct = acteurByNoAct;
    }
}
