import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Personnage} from './personnage';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PersonnagesService {

  constructor(private http: HttpClient) { }

  getPersonnages(noFilm: number): Observable<Personnage[]> {
    return this.http.get<Object>('http://localhost:8080/ressources/getPersonnagesOfFilm/' + noFilm)
      .pipe(
        map(
          persos => {
            let personnages: Personnage[];
            personnages = [];
            for (let i in persos) {
              personnages.push(new Personnage(persos[i]['nomPers'], persos[i]['noAct']));
            }
            return personnages;
          }
        )
      );
  }
}
