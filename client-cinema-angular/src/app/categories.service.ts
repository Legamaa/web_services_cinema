import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Realisateur} from './realisateur';
import {catchError, map} from 'rxjs/operators';
import {Categorie} from './categorie';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  constructor(private http: HttpClient) { }

  getCategories(): Observable<Categorie[]> {
    return this.http.get<Object[]>('http://localhost:8080/ressources/getCategories')
      .pipe(
        map(
          cats => cats.map(
            cat => new Categorie(cat['codeCat'], cat['libelleCat'])
          )
        )
      );
  }
}

