import { Component, OnInit } from '@angular/core';
import {Film} from '../film';
import {FilmsService} from '../films.service';
import {RealisateursService} from '../realisateurs.service';
import {Realisateur} from '../realisateur';
import {Categorie} from '../categorie';
import {CategoriesService} from '../categories.service';

@Component({
  selector: 'app-add-film',
  templateUrl: './add-film.component.html',
  styleUrls: ['./add-film.component.css']
})
export class AddFilmComponent implements OnInit {

  film: Film = new Film(0, 'titre', 0, '01/01/2000', 0, 0, 'CO', 1);;

  realisateurs: Realisateur[];

  categories: Categorie[];

  constructor(private filmService: FilmsService,
              private realisateurService: RealisateursService,
              private  categorieService: CategoriesService) { }

  getMaxId(films) {
    let max = 0;
    for (let film of films) {
      console.log(film);
      console.log(film.noFilm);
      if (film.noFilm > max) {
        max = film.noFilm;
      }
    }
    this.film.noFilm = max + 1;
  }

  ngOnInit() {
    this.realisateurService.getRealisateurs().subscribe( realisateurs => this.realisateurs = realisateurs );
    this.categorieService.getCategories().subscribe( cats => this.categories = cats );
    this.filmService.getFilms().subscribe(films => this.getMaxId(films) );
  }

  onSubmit() {
    this.filmService.addFilm(this.film).subscribe();
    window.location.reload();
  }

}
