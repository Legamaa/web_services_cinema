import { TestBed } from '@angular/core/testing';

import { ActeursService } from './acteurs.service';

describe('ActeursService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ActeursService = TestBed.get(ActeursService);
    expect(service).toBeTruthy();
  });
});
