import {Realisateur} from './realisateur';
import {Categorie} from './categorie';

export class Film {

  realisateur: Realisateur;

  categorie: Categorie;

  constructor(public noFilm: number,
              public titre: string,
              public duree: number,
              public dateSortie: string,
              public budget: number,
              public montantRecette: number,
              public codeCat: string,
              public noRea: number) {}
}
