import { Component, OnInit } from '@angular/core';
import {FilmsService} from '../films.service';
import {RealisateursService} from '../realisateurs.service';
import {Film} from '../film';
import {CategoriesService} from '../categories.service';
import {Categorie} from '../categorie';

@Component({
  selector: 'app-films',
  templateUrl: './films.component.html',
  styleUrls: ['./films.component.css']
})
export class FilmsComponent implements OnInit {

  films: Film[];

  currentFilm: Film;

  categories: Categorie[];

  constructor(private filmService: FilmsService,
              private realisateurService: RealisateursService,
              private categorieService: CategoriesService) { }

  ngOnInit() {
    this.getFilms();
  }

  getFilms() {
    this.categorieService.getCategories().subscribe(cats => this.categories = cats);
    this.filmService.getFilms().subscribe(films => {
      this.films = films;
      for (let film of this.films) {
        this.realisateurService.getRealisateur(film.noRea).subscribe(
          rea => film.realisateur = rea
        );
        for (let cat of this.categories) {
          if (film.codeCat === cat.codeCat) {
            film.categorie = cat;
          }
        }
      }
    });
  }

  deleteFilm(idFilm: number) {
    this.filmService.deleteFilm(idFilm).subscribe(v => this.getFilms());
  }

  getFilmInformation(film) {
    this.currentFilm = film;
  }

}
