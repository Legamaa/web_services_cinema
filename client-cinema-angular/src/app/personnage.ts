import {Acteur} from './acteur';

export class Personnage {

  acteur: Acteur;

  constructor(public nomPersonnage: string,
              public noAct: number) {}
}
