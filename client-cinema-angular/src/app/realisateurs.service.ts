import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Realisateur} from './realisateur';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RealisateursService {

  constructor(private http: HttpClient) { }

  getRealisateurs(): Observable<Realisateur[]> {
    return this.http.get<Object[]>('http://localhost:8080/ressources/getRealisateurs')
      .pipe(
        map(
          reas => reas.map(
            rea => new Realisateur(rea['noRea'], rea['nomRea'], rea['prenRea'])
          )
        )
      );
  }

  getRealisateur(noRea: number): Observable<Realisateur> {
    return this.http.get<Object>('http://localhost:8080/ressources/getRealisateur/' + noRea)
      .pipe(
        map(
            rea => new Realisateur(rea['noRea'], rea['nomRea'], rea['prenRea'])
        )
      );
  }
}

