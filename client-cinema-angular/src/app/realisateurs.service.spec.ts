import { TestBed } from '@angular/core/testing';

import { RealisateursService } from './realisateurs.service';

describe('RealisateursService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RealisateursService = TestBed.get(RealisateursService);
    expect(service).toBeTruthy();
  });
});
