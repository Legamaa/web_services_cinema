import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {Film} from './film';
import {catchError, map} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FilmsService {

  constructor(private http: HttpClient) { }

  getFilms(): Observable<Film[]> {
    return this.http.get<Object[]>('http://localhost:8080/ressources/getFilms')
      .pipe(
        map(
          films => films.map(
            film => new Film(film['noFilm'], film['titre'], film['duree'], film['dateSortie'], film['budget'], film['montantRecette'], film['codeCat'], film['noRea'])
          )
        )
      );
  }

  deleteFilm (idFilm: number): Observable<{}> {
    return this.http.post<{}>('http://localhost:8080/ressources/deleteFilm/' + idFilm, null).pipe();
  }

  addFilm (film: Film): Observable<{}> {
    return this.http.post<{}>('http://localhost:8080/ressources/addFilm', film).pipe();
  }
}
