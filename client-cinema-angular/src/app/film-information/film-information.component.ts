import {Component, Input, OnInit} from '@angular/core';
import {Film} from '../film';
import {Personnage} from '../personnage';
import {PersonnagesService} from '../personnages.service';
import {ActeursService} from '../acteurs.service';

@Component({
  selector: 'app-film-information',
  templateUrl: './film-information.component.html',
  styleUrls: ['./film-information.component.css']
})
export class FilmInformationComponent implements OnInit {

  private _film: Film;

  personnages: Personnage[];

  constructor(private personnagesService: PersonnagesService,
              private acteurService: ActeursService) { }

  ngOnInit() {
  }

  @Input()
  set film(film: Film) {
    this._film = film;
    this.onRefresh();
  }

  get film() {
    return this._film;
  }

  onRefresh() {
    this.personnagesService.getPersonnages(this.film.noFilm).subscribe(
      persos => {
        this.personnages = persos;
        for (let perso of this.personnages) {
          this.acteurService.getActeur(perso.noAct).subscribe(acteur => {
            perso.acteur = acteur;
          });
        }
      }
    );
  }

}
