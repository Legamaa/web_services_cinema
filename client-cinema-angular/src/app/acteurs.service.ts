import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import {catchError, map} from 'rxjs/operators';
import {Acteur} from './acteur';

@Injectable({
  providedIn: 'root'
})
export class ActeursService {

  constructor(private http: HttpClient) { }

  getActeur(noActeur: number): Observable<Acteur> {
    return this.http.get<Object>('http://localhost:8080/ressources/getActeur/' + noActeur)
      .pipe(
        map(
          acteur => new Acteur(acteur['nomAct'], acteur['prenAct'])
        )
      );
  }
}
