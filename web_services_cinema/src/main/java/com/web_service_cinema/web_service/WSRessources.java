package com.web_service_cinema.web_service;

import com.web_service_cinema.persistence.*;
import com.web_service_cinema.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/ressources")
public class WSRessources {

    @Autowired
    private EntityFilmRepository filmRepository;
    @Autowired
    private EntityPersonnageRepository personnageRepository;
    @Autowired
    private EntityRealisateurRepository realisateurRepository;
    @Autowired
    private EntityCategorieRepository categorieRepository;
    @Autowired
    private EntityActeurRepository acteurRepository;

    public WSRessources() {

    }

    @GetMapping("/getFilms")
    public List<FilmEntity> getFilms() {
        List<FilmEntity> films = null;
        try {
            films = this.filmRepository.findAll();
        }  catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return films;
    }

    @RequestMapping(value = "/addFilm", method = RequestMethod.POST)
    @ResponseBody
    public void addFilm(@Valid @RequestBody FilmEntity newFilm)
    {
        try {
            System.out.println(newFilm.getNoRea());
            this.filmRepository.save(newFilm);
            System.out.println("Film added");
        } catch (Exception e) {
            ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/deleteFilm/{id}", method = RequestMethod.POST)
    @ResponseBody
    public void deleteFilm(@PathVariable(value = "id") int id) {
        try {
            //Suppression des personnages du Film
            List<PersonnageEntity> listPersonnage = this.personnageRepository.findAll();
            for (PersonnageEntity perso: listPersonnage) {
                if (perso.getNoFilm() == id) {
                    this.personnageRepository.delete(perso);
                }
            }

            //Suppresion du film
            FilmEntity filmToDelete = this.filmRepository.getOne(id);
            this.filmRepository.delete(filmToDelete);
            System.out.println("Film " + id + " deleted");
        } catch (Exception e) {
            ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "/getPersonnagesOfFilm/{id}", method = RequestMethod.GET)
    @ResponseBody
    public List<PersonnageEntity> getRolesById(@PathVariable(value = "id") int id) {
        List<PersonnageEntity> personnages = null;
        try{
            personnages = personnageRepository.findByNoFilm(id);
        }catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return personnages;
    }

    @GetMapping("/getRealisateurs")
    public List<RealisateurEntity> getRealisateurs() {
        List<RealisateurEntity> realisateurs = null;
        try {
            realisateurs = this.realisateurRepository.findAll();
        }  catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return realisateurs;
    }

    @GetMapping("/getRealisateur/{id}")
    public RealisateurEntity getRealisateur(@PathVariable(value = "id") int id) {
        RealisateurEntity realisateur = null;
        try {
            realisateur = this.realisateurRepository.findById(id).get();
        }  catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return realisateur;
    }

    @GetMapping("/getCategories")
    public List<CategorieEntity> getCategories() {
        List<CategorieEntity> categories = null;
        try {
            categories = this.categorieRepository.findAll();
        }  catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return categories;
    }

    @GetMapping("/getActeur/{id}")
    public ActeurEntity getActeurById(@PathVariable(value = "id") int id) {
        ActeurEntity acteur = null;
        try{
            acteur = acteurRepository.findById(id).get();
        }catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return acteur;
    }

    @GetMapping("/getFilm/{id}")
    public FilmEntity getFilmById(@PathVariable(value = "id") int id) {
        FilmEntity film = null;
        try{
            film = filmRepository.findById(id).get();
        }catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return film;
    }

    @GetMapping("/getCat/{id}")
    public CategorieEntity getCategorieById(@PathVariable(value = "id") String id) {
        CategorieEntity categorie = null;
        try{
            categorie = categorieRepository.findByCodeCat(id);
        }catch (Exception e) {
            ResponseEntity.notFound().build();
        }
        return categorie;
    }

}
