package com.web_service_cinema.repositories;

import com.web_service_cinema.persistence.FilmEntity;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EntityFilmRepository extends JpaRepository<FilmEntity, Integer> {
}
