package com.web_service_cinema.repositories;

import com.web_service_cinema.persistence.CategorieEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface EntityCategorieRepository extends JpaRepository<CategorieEntity, String> {

    @Query("select c from CategorieEntity  c  where c.codeCat = ?1")
    CategorieEntity findByCodeCat(String codeCat);
}
