package com.web_service_cinema.repositories;

import com.web_service_cinema.persistence.ActeurEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EntityActeurRepository extends JpaRepository<ActeurEntity, Integer> {

}
