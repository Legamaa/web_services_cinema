package com.web_service_cinema.repositories;

import com.web_service_cinema.persistence.RealisateurEntity;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface EntityRealisateurRepository extends JpaRepository<RealisateurEntity, Integer> {
}