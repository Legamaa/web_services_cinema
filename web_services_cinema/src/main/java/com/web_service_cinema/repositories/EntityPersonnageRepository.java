package com.web_service_cinema.repositories;

import com.web_service_cinema.persistence.PersonnageEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

@Repository
public interface EntityPersonnageRepository extends JpaRepository<PersonnageEntity, Integer> {

    @Query("select p from PersonnageEntity  p JOIN p.filmByNoFilm where p.noFilm = ?1")
    List findByNoFilm(int nAct);
}
