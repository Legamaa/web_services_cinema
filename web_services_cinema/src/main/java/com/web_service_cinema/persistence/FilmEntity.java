package com.web_service_cinema.persistence;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.*;
import java.io.IOException;
import java.sql.Date;
import java.util.Collection;
import java.util.Objects;


@Entity
@Table(name = "film", schema = "cinema", catalog = "")
public class FilmEntity {
    private int noFilm;
    private String titre;
    private int duree;
    private Date dateSortie;
    private int budget;
    private int montantRecette;
    private int noRea;
    private String codeCat;
    private RealisateurEntity realisateurByNoRea;
    private CategorieEntity categorieByCodeCat;

    @Id
    @Column(name = "nofilm", nullable = false)
    public int getNoFilm() {
        return noFilm;
    }

    public void setNoFilm(int noFilm) {
        this.noFilm = noFilm;
    }

    @Basic
    @Column(name = "titre", nullable = false, length = 30)
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "duree", nullable = false)
    public int getDuree() {
        return duree;
    }

    public void setDuree(int duree) {
        this.duree = duree;
    }

    @Basic
    @Column(name = "datesortie", nullable = false)
    public Date getDateSortie() {
        return dateSortie;
    }

    public void setDateSortie(Date dateSortie) {
        this.dateSortie = dateSortie;
    }

    @Basic
    @Column(name = "budget", nullable = false)
    public int getBudget() {
        return budget;
    }

    public void setBudget(int budget) {
        this.budget = budget;
    }

    @Basic
    @Column(name = "montantrecette", nullable = false)
    public int getMontantRecette() {
        return montantRecette;
    }

    public void setMontantRecette(int montantRecette) {
        this.montantRecette = montantRecette;
    }

    @Basic
    @Column(name = "norea", nullable = false)
    public int getNoRea() {
        return noRea;
    }

    public void setNoRea(int noRea) {
        this.noRea = noRea;
    }

    @Basic
    @Column(name = "codecat", nullable = false, length = 2)
    public String getCodeCat() {
        return codeCat;
    }

    public void setCodeCat(String codeCat) {
        this.codeCat = codeCat;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FilmEntity that = (FilmEntity) o;
        return noFilm == that.noFilm &&
                duree == that.duree &&
                budget == that.budget &&
                montantRecette == that.montantRecette &&
                noRea == that.noRea &&
                Objects.equals(titre, that.titre) &&
                Objects.equals(dateSortie, that.dateSortie) &&
                Objects.equals(codeCat, that.codeCat);
    }

    @Override
    public int hashCode() {

        return Objects.hash(noFilm, titre, duree, dateSortie, budget, montantRecette, noRea, codeCat);
    }

    @ManyToOne
    @JoinColumn(name = "norea", referencedColumnName = "norea", nullable = false, insertable = false, updatable = false)
    public RealisateurEntity getRealisateurByNoRea() {
        return realisateurByNoRea;
    }

    public void setRealisateurByNoRea(RealisateurEntity realisateurByNoRea) {
        this.realisateurByNoRea = realisateurByNoRea;
    }

    @ManyToOne
    @JoinColumn(name = "codecat", referencedColumnName = "codecat", nullable = false, insertable = false, updatable = false)
    public CategorieEntity getCategorieByCodeCat() {
        return categorieByCodeCat;
    }

    public void setCategorieByCodeCat(CategorieEntity categorieByCodeCat) {
        this.categorieByCodeCat = categorieByCodeCat;
    }



}
