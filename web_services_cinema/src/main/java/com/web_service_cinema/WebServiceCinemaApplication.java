package com.web_service_cinema;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class WebServiceCinemaApplication {

    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(WebServiceCinemaApplication.class);
    }

    public static void main(String[] args) {
        SpringApplication.run(WebServiceCinemaApplication.class, args);
    }
}
