# Lancement de l'api et des clients
## API
L'api est une application Spring boot. Elle se trouve dans le dossier
`web_services_cinema/` et il faut lancer l'application en lançant la classe
`WebServiceCinemaApplication` qui se trouve dans le dossier
`src/main/java/com/web_service_cinema/`. Il faut faire attention que la base
de donnée MySQL soit lancée sur l'adresse à définir dans le fichier 
`application.properties` dans le dossier `src/main/java/ressources/` avec les
bon `username` et le bon `password`.

## Client Angular
Le client angular se trouve dans le dossier `client-cinema-angular/`, pour le
lancer, il suffit de se placer dans se répertoire et de lancer la commande
`ng serve` puis de se randre à l'url indiquée (normalement `localhost:4200`).

## Client JSP
Le client jsp se trouve dans le dossier `clicinema/`. Pour le lancer, il faut
l'ouvrir avec intellij, et le run normalement. Il est reglé pour tourner avec l'api
que l'on peut atteindre sur le port 8090. Pour cela, on modifie la runconfiguration
de l'api en ajoutant dans : Environnement => VM options : `-Dserver.port=8090`.